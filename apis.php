<?php
include 'utility.class.php';
$utility = new utility();

try {

	$flag = true;
	$msg = "Successfull";

	$validate = $utility->request_validate($_REQUEST);
	if ($validate == false) {
		$flag = false;
		throw new Exception("Please Input All Valid Value.");
	}

	$result = $utility->get_price($_REQUEST);
}
//catch exception
 catch (Exception $e) {
	//echo 'Message: ' . $e->getMessage();
	echo json_encode(array('flag' => $flag, 'msg' => $e->getMessage()));
	exit();
}

echo json_encode(array('flag' => $flag, 'msg' => $msg, 'result' => $result));
exit();
//echo json_encode($result);

?>