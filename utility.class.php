<?php

class utility {

	private static $day_in_month = array(
		'1' => array('month_name' => "Jan", 'days' => 31),
		'2' => array('month_name' => "Feb", 'days' => 28),
		'3' => array('month_name' => "Mar", 'days' => 31),
		'4' => array('month_name' => "Apr", 'days' => 30),
		'5' => array('month_name' => "May", 'days' => 31),
		'6' => array('month_name' => "Jun", 'days' => 30),
		'7' => array('month_name' => "Jul", 'days' => 31),
		'8' => array('month_name' => "Aug", 'days' => 31),
		'9' => array('month_name' => "Sep", 'days' => 30),
		'10' => array('month_name' => "Oct", 'days' => 31),
		'11' => array('month_name' => "Nov", 'days' => 30),
		'12' => array('month_name' => "Dec", 'days' => 31),
	);

	private $ram_for_1_studies = 0.5; //(MB) Its from input 1000 studies need 500 MB RAM.
	private $ram_price_per_hour = 0.00553; //(USD) Per Hour cost for 1 GB RAM.

	private $storage_for_1_studies = 10; //(MB) 1 study use 10 MB of storage.
	private $storage_price_per_month = 0.10; //(USD) Per Month Cost for storage.

	/**
	 * utility constructor.
	 */
	function __construct() {
		$current_year = date('yy');
		$RAM_price_per_day = $this->ram_price_per_hour * 24;
		$storage_price_per_month = $this->storage_price_per_month;
	}

	/**
	 * @param integer $no_of_studies
	 * @param array $month_data
	 * @return double|integer
	 */
	private function get_ram_cost_of_studies_month($no_of_studies, $month_data) {

		$ram_for_1_studies = $this->ram_for_1_studies;
		$RAM_price_per_day = $this->ram_price_per_hour * 24;
		$no_of_studies = $no_of_studies / $month_data['days'];

		$total_RAM_price_day = (($no_of_studies * $ram_for_1_studies) / 1000) * $RAM_price_per_day;
		return $total_RAM_price_day * $month_data['days'];
	}

	/**
	 * @param integer $no_of_studies
	 * @param array $month_data
	 * @return double|integer
	 */
	//Let assume storage will empty after day end.
	private function get_storage_cost_of_studies_month($no_of_studies, $month_data) {

		$storage_for_1_studies = $this->storage_for_1_studies;
		$storage_price_per_month = $this->storage_price_per_month;
		$no_of_studies = $no_of_studies / $month_data['days'];

		$total_storage_price_day = (($no_of_studies * $storage_for_1_studies) / 1000);
		return $total_storage_price_day * $storage_price_per_month;
	}

	/**
	 * @param array $request
	 * @return true|false
	 */
	public function request_validate($request) {
		if (empty($request)) {
			return false;
		} else if (!isset($request['study_per_day']) || $request['study_per_day'] == '') {
			return false;
		} else if (!isset($request['growth_per_month']) || $request['growth_per_month'] == '') {
			return false;
		} else if (!isset($request['months_no']) || $request['months_no'] == '') {
			return false;
		}
		return true;
	}

	/**
	 * @param integer $study_per_day
	 * @param integer $growth_per_month
	 * @param array $month_data
	 * @return false|mixed|string
	 */
	private function get_no_of_studies_month($study_per_day, $growth_per_month, $month_data) {
		$val = $month_data['days'] * $study_per_day;
		if ($growth_per_month > 0) {
			$val += ($growth_per_month / 100) * $val;
		}
		return round($val, 0);
	}

	/**
	 * @param array $data
	 * @return false|array
	 */
	public function get_price($data) {

		$result_array = [];
		$j = $data['months_no'];
		if ($data['months_no'] == 12) {
			$j = 1;
		}

		$growth_per_month = $data['growth_per_month'];
		for ($i = $j; $i <= $data['months_no']; $i++) {

			$month_data = self::$day_in_month[$i];
			$no_of_studies = $this->get_no_of_studies_month($data['study_per_day'], $data['growth_per_month'], $month_data);

			$ram_cost_of_studies = $this->get_ram_cost_of_studies_month($no_of_studies, $month_data);
			$storage_cost_of_studies = $this->get_storage_cost_of_studies_month($no_of_studies, $month_data);

			$cost = $ram_cost_of_studies + $storage_cost_of_studies;
			$result_array[] = array(
				'month' => $month_data['month_name'] . ' ' . date('Y'),
				'studies' => $no_of_studies,
				'cost' => round($cost, 2),
			);
		}

		return $result_array;
	}
}

?>



