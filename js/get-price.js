var vm = new Vue({
       el: '#vue_data',
       data: {
          errors: [],
          //table_head : "Ria",
          table_body  : [],
          // table_body  : [{'month':'12','studies':'12','cost':'12'}],
          htmlcontent : "<div><h1>Vue Js Template</h1></div>",
          seen: false,
          study_per_day:'',
          growth_per_month:'',
          months_no:''
       },

       mounted: function (event) {
        //this.$refs.submitBtn.click();
        //console.log(this.table_body);
       },
       methods: {
            searchForPrice: function (event) {
            this.errors = [];
            if (!this.study_per_day) {
              this.errors.push('Study Number Required.');
              return false;
            }           
            if (!this.months_no) {
              this.errors.push('Months Number Required.');
              return false;
            }
            vm.seen = true;
            //fetch(`apis.php?apiName=${this.months_no}`)
            $.getJSON(`apis.php?study_per_day=${encodeURIComponent(this.study_per_day)}&growth_per_month=${encodeURIComponent(this.growth_per_month)}&months_no=${encodeURIComponent(this.months_no)}`,function (json) {
                if(json.flag == false){
                    this.errors.push(json.msg);
                    vm.seen = false;
                    return false;
                }
                vm.table_body = json.result;
                vm.seen = false;
            });
        },
        isNumber: function(evt) {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            evt.preventDefault();;
          } else {
            return true;
          }
        }
      }
});
